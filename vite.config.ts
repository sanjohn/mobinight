/// <reference types="vitest" />

import { defineConfig, loadEnv, ConfigEnv } from 'vite';
import vue from '@vitejs/plugin-vue'
import { resolve } from 'path';


const pathResolve = (dir: string) => {
	return resolve(__dirname, '.', dir);
};

const alias: Record<string, string> = {
	'@': pathResolve('./src'),
	'@public': pathResolve('./public'),
	// 'vue-i18n': 'vue-i18n/dist/vue-i18n.cjs.js',
};
const viteConfig = defineConfig((mode: ConfigEnv) => {
	const env = loadEnv(mode.mode, process.cwd());
	let isProduction = false;
	if (mode.mode === 'server') {
		isProduction = true;
	}
	return {
		plugins: [
			vue(),
		],
		test: {
			globals: true,
			environment: 'jsdom'
		},
		root: process.cwd(),
		resolve: { alias },
		base: mode.command === 'serve' ? './' : env.VITE_PUBLIC_PATH,
		optimizeDeps: { exclude: ['vue-demi'] },
		server: {
			host: '0.0.0.0',
			port: env.VITE_PORT as unknown as number,
			open: JSON.parse(env.VITE_OPEN),
			hmr: true,
			proxy: {
				'/api': {
					target: isProduction ? 'https://api.tovna24.com/api' : 'http://127.0.0.1:8000',
					changeOrigin: true,
					rewrite: (path: string) => path.replace(/^\/api/, ''), // Remove /api prefix
				},
				'/uploads': {
					target: isProduction ? 'https://api.tovna24.com/uploads' : 'http://127.0.0.1:8000/uploads',
					changeOrigin: true,
					rewrite: (path: string) => path.replace(/^\/uploads/, ''),
				},
			},
		},
	};
});

export default viteConfig;