import { createI18n } from 'vue-i18n';
import pinia from '@/store';
import { storeToRefs } from 'pinia';
import en from './lang/en';
import cn from './lang/cn';
import kh from './lang/kh';
import { useStore } from '@/store/store';

const messages = {
    en,
    cn,
    kh,
};

const store = useStore(pinia)

const switchLanguage = (lang = "en") => {
    store.setLang(lang);
};

const { lang } = storeToRefs(store);
const i18n = createI18n({
    locale: lang.value, // set locale
    fallbackLocale: 'en', // set fallback locale
    messages, // set locale messages
});


export { i18n, switchLanguage };