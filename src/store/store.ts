import { defineStore } from 'pinia';
import { ICategory } from '@/models/ICategory';

/**
  * User Info
  * @methods useStore Set user information
 */
export const useStore = defineStore('store', {
	state: () => ({
        expDate: '',
		categories: <ICategory[]>[],
		lang: 'en',
	}),
	actions: {
		setExpDate(date: string) {
			this.expDate = date
		},
		setCategories(categories: any) {
			this.categories = categories;
		},
		setLang(lang: string) {
			this.lang = lang;
		}
	},
	persist: true,
});
