import { defineStore } from 'pinia';
import Cookies from 'js-cookie';
import { Session } from '@/utils/storage';

/**
 * User Info
 * @methods setUserInfos Set user information
 */
export const useUserInfo = defineStore('userInfo', {
	state: (): UserInfosState => ({
		userInfos: {
			userName: '',
			photo: '',
			time: 0,
			roles: [],
			authBtnList: [],
			uuid: '',
		},
	}),
	actions: {
		async setUserInfos() {
			// Store user information in browser cache
			if (Session.get('userInfo')) {
				this.userInfos = Session.get('userInfo');
			} else {
				const userInfos = <UserInfos>await this.getApiUserInfo();
				this.userInfos = userInfos;
			}
		},
		// Analog interface data
		// https://gitee.com/lyt-top/vue-next-admin/issues/I5F1HP
		async getApiUserInfo() {
			return new Promise((resolve) => {
				setTimeout(() => {
					// When simulating data and requesting interfaces, remember to delete redundant code and introduce corresponding dependencies.
					const userName = Cookies.get('userName');
					const uuid = Cookies.get('uuid');
					// simulated data
					let defaultRoles: Array<string> = [];
					let defaultAuthBtnList: Array<string> = [];
					// admin Page permission identifier, corresponding to routing meta.roles, used to control the display/hiding of routing
					let adminRoles: Array<string> = ['admin'];
					// admin Button permission identifier
					let adminAuthBtnList: Array<string> = ['btn.add', 'btn.del', 'btn.edit', 'btn.link'];
					// test Page permission identifier, corresponding to routing meta.roles, used to control the display/hiding of routing
					let testRoles: Array<string> = ['common'];
					// test Button permission identifier
					let testAuthBtnList: Array<string> = ['btn.add', 'btn.link'];
					// Different users simulate different user permissions
					if (userName === 'admin') {
						defaultRoles = adminRoles;
						defaultAuthBtnList = adminAuthBtnList;
					} else {
						defaultRoles = testRoles;
						defaultAuthBtnList = testAuthBtnList;
					}
					// User information simulation data
					const userInfos = {
						userName: userName,
						photo:
							userName === 'admin'
								? 'https://img2.baidu.com/it/u=1978192862,2048448374&fm=253&fmt=auto&app=138&f=JPEG?w=504&h=500'
								: 'https://img2.baidu.com/it/u=2370931438,70387529&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500',
						time: new Date().getTime(),
						roles: defaultRoles,
						authBtnList: defaultAuthBtnList,
						uuid: uuid,
					};
					Session.set('userInfo', userInfos);
					resolve(userInfos);
				}, 0);
			});
		},
	},
});
