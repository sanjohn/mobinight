import service from '~/utils/request';

export default function useAuthApi() {
    return {
        login(params?: object) {
            return service.post('auth/login', params)
        },
        saveUser(params?: object) {
            return service.post('auth/saveUser', params)
        },
        saveAdmin(params?: object) {
            return service.post('auth/saveAdmin', params)
        },
        saveInfo(params?: object) {
            return service.post('auth/saveInfo', params)
        },
        refresh(params?: object) {
            return service.post('auth/refresh', params)
        },
        getInfo: (params?: object) => {
			return service({
				url: 'auth/me',
				method: 'get',
				params,
			});
		},
        adminAcc: (params?: object) => {
			return service({
				url: 'auth/adminAcc',
				method: 'get',
				params,
			});
		},
        getPsSaved: (params?: object) => {
			return service({
				url: 'getPsSaved',
				method: 'get',
				params,
			});
		},
        getMyBooking: (params?: object) => {
			return service({
				url: 'getMyBooking',
				method: 'get',
				params,
			});
		},
        deleteSaved: (params?: object) => {
			return service.post('deleteSaved', params);
		},
        logout(params?: object) {
            return service({
                url: 'auth/logout',
                method: 'post',
                data: params,
            });
        },
        isExpire(params?: object) {
            return service({
                url: 'auth/isExpire',
                method: 'get',
                data: params,
            });
        },
    }
}