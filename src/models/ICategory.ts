import functionHelper from "../libraries/formatHelpers/functionHelper";
import type IBase from "./IBase";

interface ICategory extends IBase{
    name: string,
    key: string,
    image: string,
}
class Category implements ICategory {
    is_visible: boolean;
    created_at: string;
    updated_at: string
    name!: string;
    key!: string;
    image!: string;
    id!: number;
    sort!: number;
    is_delete!: boolean;
    constructor(init: ICategory) {
        Object.assign(this, init);
        this.created_at = functionHelper.dateStringTo12HourWithTime(init.created_at)
        this.updated_at = functionHelper.dateStringTo12HourWithTime(init.updated_at);
        this.is_visible = init.is_visible
    }
}
export {
    type ICategory,
    Category,
}