import type IBase from "./IBase";
import functionHelper from "~/libraries/formatHelpers/functionHelper";

interface IConfiguration extends IBase{
    id: number,
    appName: string,
    key: string,
    input: string,
    value: string,
    sort: number,
    type: string,
}
class Configuration implements IConfiguration {
    appName!: string;
    created_at: string;
    id!: number;
    input!: string;
    is_delete!: boolean;
    is_visible!: boolean;
    key!: string;
    sort!: number;
    type!: string;
    updated_at: string;
    value!: string;
    constructor(init: IConfiguration) {
        Object.assign(this, init)
        this.created_at = functionHelper.dateStringTo12HourWithTime(init.created_at)
        this.updated_at = functionHelper.dateStringTo12HourWithTime(init.updated_at);
    }
}
export {
    type IConfiguration,
    Configuration,
}