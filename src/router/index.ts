import { createRouter, createWebHistory } from '@ionic/vue-router';
import { ComonnRoutes, AuthorizeRoutes, ErrorRoutes } from './routes';
import { Session } from '@/utils/storage';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [...ComonnRoutes, ...AuthorizeRoutes, ...ErrorRoutes],
})

router.beforeEach((to, from, next) => {
  const token = Session.get('token');

  if (to.meta.isAuth && !token) {
    next({ name: 'Login' });  // Redirect to login page
  } else {
    next();
  }
});

export default router
