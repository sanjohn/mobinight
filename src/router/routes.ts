import { RouteRecordRaw } from 'vue-router';
import Layout from '@/layouts/layout-page.vue';

//Extend RouteMeta interface
declare module 'vue-router' {
    interface RouteMeta {
        title?: string;
        isLink?: string;
        isHide?: boolean;
        isKeepAlive?: boolean;
        isAffix?: boolean;
        isIframe?: boolean;
        roles?: string[];
        icon?: string;
        isAuth?: boolean;
    }
}

export const ComonnRoutes: Array<RouteRecordRaw> = [
    {
        path: '/',
        redirect: '/home'
    },
    {
        path: '/',
        component: Layout,
        children: [
            {
                path: '',
                redirect: '/home',
            },

            {
                path: 'home',
                component: () => import('@/pages/HomePage.vue'),
            },
            {
                path: 'radio',
                component: () => import('@/pages/HomePage.vue'),
            },
            {
                path: 'library',
                component: () => import('@/pages/HomePage.vue'),
            },
            {
                path: 'search',
                component: () => import('@/pages/HomePage.vue'),
            },
            {
                path: 'login',
                component: () => import('@/pages/HomePage.vue'),
            },
            {
                path: 'register',
                component: () => import('@/pages/HomePage.vue'),
            },
        ],
    },
];

export const AuthorizeRoutes = [
    {
        path: '/:path(.*)*',
        name: 'notFound',
        component: () => import('@/pages/error/404.vue'),
        meta: {
            title: 'Not found 404!',
            isAuth: true,
        },
    },
    {
        path: '/401',
        name: 'noPower',
        component: () => import('@/pages/error/401.vue'),
        meta: {
            title: 'Error 401!',
            isAuth: true,
        },
    },
];

export const ErrorRoutes = [
    {
        path: '/:path(.*)*',
        name: 'notFound',
        component: () => import('@/pages/error/404.vue'),
        meta: {
            title: 'Not found 404!',
            isHide: true,
        },
    },
    {
        path: '/401',
        name: 'noPower',
        component: () => import('@/pages/error/401.vue'),
        meta: {
            title: 'Error 401!',
            isHide: true,
        },
    },
];