import axios, { type AxiosInstance } from 'axios';
import { Session } from '@/utils/storage';
import qs from 'qs';
import { useStore } from '@/store/store';
import { alertController } from '@ionic/vue';
import { useRouter } from 'vue-router';

const presentAlert = async (params: any) => {
    const alert = await alertController.create({
        header: params.header,
        subHeader: params.subHeader,
        message: params.message,
        buttons: ['Ok'],
    });

    await alert.present();
};
//Configure a new axios instance
const service: AxiosInstance = axios.create({
    baseURL: '/api/',
    timeout: 50000,
    headers: { 'Content-Type': 'application/json' },
    paramsSerializer: {
        serialize(params) {
            return qs.stringify(params, { allowDots: true });
        },
    },
});

// Add request interceptor
service.interceptors.request.use(
    (config) => {
        // What to do before sending a request token
        if (Session.get('token')) {
            config.headers!['Authorization'] = `${Session.get('token')}`;
        }
        return config;
    },
    (error) => {
        // What to do about request errors
        return Promise.reject(error);
    }
);
// Add response interceptor
service.interceptors.response.use(
    (response) => {
        // Do something with the response data
        const res = response.data;
        if (res.code && res.code !== 200) {
            // `token` Expired or the account has been logged in elsewhere
            if (res.code === -10004 || res.code === -20004) {
                Session.clear(); // Clear all temporary browser caches
                const router = useRouter();
                let msgObj = {
                    header: '',
                    subHeader: '',
                    message: ''
                }
                if (useStore().lang === 'en') {
                    msgObj.header = 'Hint';
                    msgObj.message = 'Please login!';
                } else if (useStore().lang === 'kh') {
                    msgObj.header = 'ព័ត៌មានជំនួយ';
                    msgObj.message = 'សូមចូលគណនី';
                } else {
                    msgObj.header = '提示';
                    msgObj.message = '请登录，请登录';
                }
                presentAlert(msgObj).then(() => {
                    router.push('/account/login');
                }).catch(() => { })

            } else if (res.code === -10001) {
                presentAlert(res.message)
            }
            return response.data;
        } else {
            return res;
        }
    },
    (error) => {
        // Do something about the response error
        if (error.message.indexOf('timeout') != -1) {
            presentAlert({ header: 'Timeout', message: 'Network timeout!' })
        } else if (error.message == 'Network Error') {
            presentAlert({ header: 'Network Error', message: 'Network connection error!' })
        } else {
            if (error.response.data) presentAlert({ header: error.response.statusText, message: error.response.statusText });
            else presentAlert({ header: 'Not found!', message: 'Interface path not found' });
        }
        return Promise.reject(error);
    }
);

// Export axios instance
export default service;
