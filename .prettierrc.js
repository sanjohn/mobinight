module.exports = {
	// What is the maximum number of characters in a line?
	printWidth: 150,
	// Specify the number of spaces per indentation level
	tabWidth: 2,
	// Use tabs instead of spaces to indent lines
	useTabs: true,
	// Print a semicolon at the end of the statement
	semi: true,
	// Use single quotes instead of double quotes
	singleQuote: true,
	// The time to change the reference object attribute. Optional value: "<as-needed|consistent|preserve>"
	quoteProps: 'as-needed',
	// Use single quotes instead of double quotes in JSX
	jsxSingleQuote: false,
	// Print trailing commas when possible on multiple lines. (For example, a single-line array will never have a trailing comma.) Optional value "<none|es5|all>", default none
	trailingComma: 'es5',
	// Print spaces between brackets in object literals
	bracketSpacing: true,
	// jsx The reverse angle brackets of the label need to be wrapped
	jsxBracketSameLine: true,
	// Include parentheses around individual arrow function parameters always：(x) => x \ avoid：x => x
	arrowParens: 'always',
	// These two options can be used to format code that starts and ends with a given character offset (inclusive and exclusive respectively).
	rangeStart: 0,
	rangeEnd: Infinity,
	// Specify the parser to use. No need to write the beginning of the file. @prettier
	requirePragma: false,
	// No need to automatically insert at the beginning of the file @prettier
	insertPragma: false,
	// Use default line breaking criteria always\never\preserve
	proseWrap: 'preserve',
	// Specify global whitespace sensitivity for HTML files css\strict\ignore
	htmlWhitespaceSensitivity: 'css',
	// Vue file script and style tag indentation
	vueIndentScriptAndStyle: false,
	// Line breaks use lf at the end. Optional value is "<auto|lf|crlf|cr>"
	endOfLine: 'lf',
};
